package org.taruts.djig.example.app;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.taruts.djig.core.DynamicProject;
import org.taruts.djig.core.childContext.DynamicProjectContextManager;

@TestConfiguration
public class OurTestConfiguration {

    /**
     * Replacing the DJiG's DynamicProjectContextManager with an implementation that does not create child contexts
     * from the dynamic project sources.
     * Note that this way the dynamic proxies of those dynamic contexts in the main context are left
     * with their delegates not set.
     */
    @Bean
    @Primary
    DynamicProjectContextManager testDynamicProjectContextManager() {
        return new DynamicProjectContextManager() {
            @Override
            public void init(DynamicProject dynamicProject) {

            }
        };
    }
}
